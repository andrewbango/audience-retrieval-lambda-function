const axios = require("axios");
const aws = require("aws-sdk");
const {
  AWS_REGION,
  AWS_FB_API_SECRET_ID,
  FB_AD_ACCOUNT_IDS,
  FB_GRAPH_API_URL,
} = require("./constants");

const getFacebookAPIToken = () => {
  const secretsManager = new aws.SecretsManager({ region: AWS_REGION });
  return new Promise((resolve, reject) => {
    secretsManager.getSecretValue(
      {
        SecretId: AWS_FB_API_SECRET_ID,
      },
      (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(JSON.parse(data.SecretString));
        }
      }
    );
  });
};

exports.handler = async (event, context, callback) => {
  console.log("Function loaded");
  try {
    const { facebookApiToken } = await getFacebookAPIToken();
    const response = await axios.get(
      `${FB_GRAPH_API_URL}/${FB_AD_ACCOUNT_IDS["TEST Bango Audiences"]}/customaudiences`,
      {
        params: {
          access_token: facebookApiToken,
          fields: [
            "id",
            "name",
            "description",
            "approximate_count_lower_bound",
            "approximate_count_upper_bound",
            "delivery_status",
          ].join(","),
        },
      }
    );
    const audienceData = response.data.data.map((audience) => {
      const {
        id,
        name,
        description,
        approximate_count_lower_bound,
        approximate_count_upper_bound,
        delivery_status,
      } = audience;
      const bangoAudienceParsed = description.match(/\*\*\*(.*?)\*\*\*/);
      const bangoAudienceId = Array.isArray(bangoAudienceParsed)
        ? bangoAudienceParsed[1]
        : "";
      return {
        id,
        name,
        bangoAudienceId,
        description: description.replace(/\*\*\*(.*?)\*\*\*/g, "").trim(),
        approximate_count_lower_bound,
        approximate_count_upper_bound,
        delivery_status,
      };
    });
    console.log("Audience data", audienceData);
    // try {
    //   axios.post("https://test.bango.ai/facebook-webhook", audienceData);
    // } catch (error) {
    //   console.log(error);
    //   callback(Error(`Could not retrieve Facebook audiences, aborting`, error));
    // }
    // We could run this every 5(?) minutes via a cron job and could then
    // call a special URL on Bango.ai that would update the audience statuses in the db
    callback(null, 200);
  } catch (error) {
    console.log(error);
    callback(Error(`Could not retrieve Facebook audiences, aborting`, error));
  }
};
