const AWS_REGION = "eu-west-2";
const AWS_FB_API_SECRET_ID =
  "arn:aws:secretsmanager:eu-west-2:660717655642:secret:facebookApiToken-e1m95d";
// Just the test one to start with
const FB_AD_ACCOUNT_IDS = {
  "TEST Bango Audiences": "act_742157426503268",
};

const FB_GRAPH_API_URL = "https://graph.facebook.com/v12.0";

module.exports = {
  AWS_REGION,
  AWS_FB_API_SECRET_ID,
  FB_AD_ACCOUNT_IDS,
  FB_GRAPH_API_URL,
};
